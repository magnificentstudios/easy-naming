var app = angular.module("easyNaming", []);
app.controller("namingController", function($scope, $http) {
	$scope.prepositionModel = true;
	$scope.prepositionList = [ "FOR", "WITH", "OR" ];
	$http.get('http://www.magnificentstudio.org/easynaming/script/convention_json.json')
			.success(function(data) {
				$scope.allNames = data;
			});
	$scope.changeName = function() {
		// $scope.formattedText = $scope.unformattedText;
		$scope.unformattedTextArray = $scope.unformattedText.split(" ");
		$scope.formattedText = "";
		for (i = 0; i < $scope.unformattedTextArray.length; i++) {
			if ($scope.prepositionModel
					&& $scope.isInPreposition($scope.unformattedTextArray[i]
							.toUpperCase())) {
				continue;
			}
			$scope.formattedText = $scope.formattedText.toUpperCase()
					+ $scope.findAbbreviation($scope.unformattedTextArray[i]
							.toUpperCase());

			// $scope.formattedText = $scope.formattedText.toUpperCase()
			// + $scope.unformattedTextArray[i].toUpperCase();
			//			
			if (i != ($scope.unformattedTextArray.length - 1)) {
				$scope.formattedText = $scope.formattedText + "_";
			}
		}
	};
	$scope.findAbbreviation = function(unformattedText) {

		for (var k = 0; k < $scope.allNames.length; k++) {
			if ($scope.allNames[k].Name == unformattedText) {
				return $scope.allNames[k].Short;
			}
		}
		return unformattedText;
	};
	$scope.isInPreposition = function(unformattedText) {
		for (var k = 0; k < $scope.prepositionList.length; k++) {
			if ($scope.prepositionList[k] == unformattedText) {
				return true;
			}
		}
		return false;
	};

});